<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 */
class CategoryTranslation
{
    use ORMBehaviors\Translatable\Translation;


    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $description;


    /**
     * @var int
     *
     * @ORM\Column(name="translatable_id", type="integer")
     */
    private $translatable_id;


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param  string
     * @return null
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getTranslatableId(): int
    {
        return $this->translatable_id;
    }
}
