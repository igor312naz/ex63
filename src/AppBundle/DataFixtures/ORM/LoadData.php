<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data []= 'ttt';
        $category = new Category();
        $category->translate('ru')->setDescription($data['ruDesc']);


        $manager->persist($category);

        $manager->flush();
    }

    //не успел вообщем :)
}
