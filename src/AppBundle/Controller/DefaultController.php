<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('ruDesc', TextType::class, ['label' => 'Фраза на русском'])
            ->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $category = new Category();
            $category->translate('ru')->setDescription($data['ruDesc']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);

            $category->mergeNewTranslations();
            $em->flush();
        }

        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findAll();
        dump($categories[0]);
        return $this->render('@App/Base/index.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/trans/{id}",requirements={"id": "\d+"}, name="trans")
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function transAction(int $id)
    {

        $trans = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->getText($id);

        dump($trans);
        return $this->render('@App/Base/trans.html.twig',array(
            'trans'=>$trans
        ));

    }
}

